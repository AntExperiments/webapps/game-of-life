// Update cache names any time any of the cached files change.
const CACHE_NAME = 'gay-v2';

// Add list of files to cache here.
const FILES_TO_CACHE = [
  './offline.html',
  './',
  './index.html',
  './dist/game.js',
  './dist/config.json',
  './dist/index.js',
  './dist/config.json.proxy.js',
  './index.css',
  './icons/192x192.png',
  './icons/144x144.png',
  './icons/72x72.png',
  './icons/96x96.png',
  './icons/48x48.png',
  './manifest.json'
];

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');

  evt.waitUntil(
      caches.open(CACHE_NAME).then((cache) => {
        console.log('[ServiceWorker] Pre-caching offline page');
        return cache.addAll(FILES_TO_CACHE);
      })
  );

  self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // Remove previous cached data from disk.
  evt.waitUntil(
      caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
          if (key !== CACHE_NAME) {
            console.log('[ServiceWorker] Removing old cache', key);
            return caches.delete(key);
          }
        }));
      })
  );

  self.clients.claim();
});

// self.addEventListener('fetch', (evt) => {
//   console.log('[ServiceWorker] Fetch', evt.request.url);
//   // Add fetch event handler here.
//   if (evt.request.mode !== 'navigate') {
//     // Not a page navigation, bail.
//     return;
//   }
//   evt.respondWith(
//     fetch(evt.request)
//         .catch(() => {
//           return caches.open(CACHE_NAME)
//               .then((cache) => {
//                 return cache.match('offline.html');
//               });
//         })
//   );
// })
self.addEventListener('fetch', e => {
  console.log('[ServiceWorker] Fetch', e.request.url);
  e.respondWith(
    // Try the cache
    caches.match(e.request).then(function(response) {
      // Fall back to e
      return response || fetch(e.request);
    }).catch(function() {
      // If both fail, show a generic fallback:
      return caches.match('/offline.html');
      // However, in reality you'd have many different
      // fallbacks, depending on URL & headers.
      // Eg, a fallback silhouette image for avatars.
    })
  );
})