import GameController from './game'

const button = document.querySelector('#runButton') || new HTMLElement
const toggle = document.querySelector('#toggleAuto') || new HTMLElement

const widthInput = document.querySelector('#widthInput') || new HTMLElement
const heightInput = document.querySelector('#heightInput') || new HTMLElement

const reinitialize = () => {
  const canvas = document.querySelector('canvas') || new HTMLCanvasElement

  const game = new GameController(canvas, Number(widthInput.value), Number(heightInput.value))
  button.onclick = () => game.tick()
  toggle.onclick = () => {
    if (game.isAuto == true) {
      toggle.innerText = "Run Automatically"
      game.isAuto = false
    }
    else {
      toggle.innerText = "Stop auto"
      game.isAuto = true
    }
    game.tick()
  }
}

widthInput.addEventListener("change", reinitialize)
heightInput.addEventListener("change", reinitialize)
reinitialize()

export {};