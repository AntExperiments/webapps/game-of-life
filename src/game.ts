import config from './config.json'

class GameController {
    width
    height

    canvas
    context
    grid: boolean[][]
    isAuto = false

    constructor(canvas : HTMLCanvasElement, width: number, height: number) {
        //#region Initiate
        this.canvas = canvas
        this.context = canvas.getContext("2d")

        this.width = width || 15
        this.height = height || 10

        this.canvas.width = this.width * (config.pixelSize + 2 * config.margin) - config.margin
        this.canvas.height = this.height * (config.pixelSize + 2 * config.margin) - config.margin

        this.grid = new Array(this.width)
        .fill(false)
        .map(() => new Array(this.height)
        .fill(false));

        //#endregion

        //#region Toggling
        const handleMouse = (e: MouseEvent) => {
            this.canvas.addEventListener("mousemove", handleMouse)

            let rect = this.canvas.getBoundingClientRect();
            let x = Math.floor( (e.clientX - rect.left) / (config.pixelSize + 2 * config.margin) )
            let y = Math.floor( (e.clientY - rect.top)  / (config.pixelSize + 2 * config.margin) )

            this.toggleCell([x,y], e.buttons == 1)
        }

        this.canvas.addEventListener("contextmenu", handleMouse)
        this.canvas.addEventListener("mousedown", handleMouse)

        this.canvas.addEventListener("mouseup", () => {
            this.canvas.removeEventListener("mousemove", handleMouse)
        })
        //#endregion

        this.tick()
        // setInterval(() => this.tick(), config.tickSpeed)
    }

    toggleCell(gridCords: Array<number>, state: boolean) {
        this.grid[gridCords[0]][gridCords[1]] = state
        this.draw();
    }
    setCell(x: number, y: number, state: boolean) {
        this.grid[x][y] = state
    }

    public tick() {
        const oldState = JSON.parse(JSON.stringify(this.grid));

        const isAdjacent = (x: number, y: number) => {
            try {
                return oldState[x][y] ? 1 : 0
            }
            catch {
                return 0
            }
        }

        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                //  -1,+1   0, 1    +1,+1
                //  -1, 0   0, 0    +1, 0
                //  -1,-1   0,-1    +1,-1

                const adjacencyTopLeft      = isAdjacent(i - 1, j + 1)
                const adjacencyTop          = isAdjacent(i + 0, j + 1)
                const adjacencyTopRight     = isAdjacent(i + 1, j + 1)
                
                const adjacencyLeft         = isAdjacent(i - 1, j + 0)
                const adjacencyRight        = isAdjacent(i + 1, j + 0)

                const adjacencyBottomLeft   = isAdjacent(i - 1, j - 1)
                const adjacencyBottom       = isAdjacent(i + 0, j - 1)
                const adjacencyBottomRight  = isAdjacent(i + 1, j - 1)

                const adjacentCells = adjacencyTopLeft + adjacencyTop + adjacencyTopRight + adjacencyRight + adjacencyBottomRight + adjacencyBottom + adjacencyBottomLeft + adjacencyLeft

                // // More compact way of the below function
                // let result = oldState[i][j];
                // if (oldState[i][j] == true) {
                //     if  ( adjacentCells >= 4 || adjacentCells <= 1 )
                //         result = false;
                // } else {
                //     if  ( adjacentCells == 3 )
                //         result = true;
                // }
                // this.setCell(i, j, result)
                // console.log(`x:${i} y:${j} adjacent:${adjacentCells}\t ${oldState[i][j]} -> ${result}`)

                // Each dead cell adjacent to exactly three live neighbors will become live in the next generation
                if  (
                        oldState[i][j] == false &&
                        adjacentCells == 3
                    )
                    this.setCell(i, j, true)

                // Each live cell with one or fewer live neighbors will die in the next generation
                else if  (
                    oldState[i][j] == true &&
                    adjacentCells <= 1
                )
                this.setCell(i, j, false)

                // Each live cell with four or more live neighbors will die in the next generation
                else if  (
                    oldState[i][j] == true &&
                    adjacentCells >= 4
                )
                this.setCell(i, j, false)

                // Each live cell with either two or three live neighbors will remain alive for the next generation
                if  (
                    this.grid[i][j] == true &&
                    (adjacentCells == 2 || adjacentCells == 3)
                )
                this.setCell(i, j, true)
            }
        }

        this.draw()

        if (this.isAuto == true)
            setTimeout(() => this.tick(), (100 - document.querySelector("#speedSlider")?.value) * 2 || 100);
    }

    draw() {
        const c = this.context || new CanvasRenderingContext2D

        this.draw_background(c)

        for (let i = 0; i < this.width; i++) {
            for (let j = 0; j < this.height; j++) {
                this.draw_cell(c, [i,j])
            }
        }
    }

    draw_cell(c: CanvasRenderingContext2D, gridCords: Array<number>) {
        console.assert(
            gridCords[0] <= this.width     &&  gridCords[0] >= 0 &&
            gridCords[1] <= this.height    &&  gridCords[1] >= 0,
            "Position of cell is out of bounds"
        )

        c.beginPath();
        c.rect(
            gridCords[0] * (config.pixelSize + 2 * config.margin),
            gridCords[1] * (config.pixelSize + 2 * config.margin),
            config.pixelSize,
            config.pixelSize
        );
        c.fillStyle = this.grid[gridCords[0]][gridCords[1]] ? `#${config.colors.foreground.active}` : `#${config.colors.foreground.disabled}`;
        c.fill(); 
    }


    draw_background(c: CanvasRenderingContext2D, color = null) {
        c.beginPath();
        c.rect(0, 0, this.canvas.width, this.canvas.height);
        c.fillStyle = color || `#${config.colors.background}`;
        c.fill(); 
    }
}

export default GameController;